from django.contrib import admin
from django.urls import path
from inventory_manager import views

urlpatterns = [
    path("", views.login_m, name="login_m"),
    path("login/", views.login_m, name="login_m"),
    path("logout/", views.logout_m, name="logout_m"),
    path("register/", views.register, name="register"),
    path("products/", views.products, name="Dashboard"),
    path("add/",views.add, name="add"),
    path("delete_item/<int:id>", views.delete_item, name="delete-item"),
    path("update/<int:id>", views.update, name="update"),
    path("edit", views.edit, name="edit"),
    path("get-names/", views.get_names,name="get-names"),
    path("sales_update/",views.sales_update, name="sales_update"),
]
