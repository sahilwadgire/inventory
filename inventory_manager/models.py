from django.db import models
from django.contrib.auth.models import User
User._meta.get_field('email')._unique = True

class inventory(models.Model):
    Name = models.CharField(max_length=100, null=False, blank=False)
    sold = models.IntegerField(null=False, blank=False)
    in_stock = models.IntegerField(null=False, blank=False)
    price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=False)
    revenue = models.DecimalField(max_digits=25, decimal_places=2, null=True, blank=False)
    discount = models.DecimalField(max_digits=25, decimal_places=2, null=True, blank=False)
    per = models.DecimalField(max_digits=25, decimal_places=2, null=True, blank=False)
    active = models.BooleanField(default=True, null=True)
    
# Create your models here.
