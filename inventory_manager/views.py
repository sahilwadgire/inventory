from django.shortcuts import render
from django.shortcuts import redirect
from django.http import JsonResponse
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .models import inventory
from .forms import CreateUserForm 

# Create your views here.

def login_m(request):   
    if request.user.is_authenticated:
        return redirect('/products')
    else:
        if request.method == "POST":
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('/products')
            else:
                messages.info(request, "Username or Password is incorrect")

        context={
            "title":"Login",

        }
        return render(request, "inventory_manager/login.html", context)

def logout_m(request):
    logout(request)
    return redirect('/')


def register(request):
    if request.user.is_authenticated:
        return redirect('/products')
    else:
        form = CreateUserForm()

        if request.method=="POST":
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, "Account was created for " + user)
                print("messages.success")
                return redirect("/")

        context={
            "title":"Register",
            "form":form
        }
        return render(request, "inventory_manager/register.html", context)

@login_required(login_url='/')
def products(request):
    inventories = inventory.objects.all().order_by('id')
    count = 0
    sale = 0
    out_of_stock = 0
    t_items = 0
    t_sold = 0
    t_worth = 0
    for i in inventories:
        count=count+1
        sale = sale + i.revenue
        t_items = t_items + i.in_stock
        t_sold = t_sold + i.sold
        t_worth = t_worth + i.in_stock*i.price
    context={
        "title":"Products",
        "inventories":inventories,
        "count":count,
        "sale":sale,
        "out_of_stock":out_of_stock,
        "t_items":t_items,
        "t_sold":t_sold,
        "t_worth":t_worth,
    }
    return render(request, "inventory_manager/products.html", context)

@login_required(login_url='/')
def add(request):
    flag = 0
    try:
        if request.method == "POST":
            n = request.POST.get('name')
            data = inventory.objects.all()
            for i in data:
                if(i.Name == n):
                    flag = 1
            if flag!=1:
                name = request.POST.get('name')
                sold = int(request.POST.get('sold'))
                stock = int(request.POST.get('stock_left'))
                price = float(request.POST.get('price'))
                discount = float(request.POST.get('discount'))
                revenue = price*sold
                active = request.POST.get('yes_no')
                per = 100-sold/stock+sold*100
                print("inside add")
                en=inventory(Name=name, sold=sold, in_stock=stock, price=price, discount=discount, revenue=revenue, active=active, per = per)
                print("after add")
                print(en)
                en.save()
                context={
                    "title":"Add Product",
                    "msg":"1"
                }
                return render(request, "inventory_manager/add.html", context)
            else:
                context={
                    "title":"Add Product",
                    "msg":"0"
                }
                return render(request, "inventory_manager/add.html", context)
    except:
        pass
    context={
        "title":"Add Product",
        "msg":""
    }
    return render(request, "inventory_manager/add.html", context)

@login_required(login_url='/')
def delete_item(request,id):
    item = inventory.objects.get(id=id)
    item.delete()
    return redirect("/products")

@login_required(login_url='/')  
def update(request, id):
    obj = inventory.objects.get(id=id)
    context={
        "title":"Update",
        "item":obj,
    }
    return render(request, "inventory_manager/update.html", context)

@login_required(login_url='/')
def edit(request):
    flag = 0
    try:
        if request.method == "GET":
            n = request.GET.get('name')
            data = inventory.objects.filter(Name=n)
            context={
                "title":"Update",
                "data":data,
            }
            return render(request, "inventory_manager/update.html", context)

        if request.method == "POST":
            print("inside post methord")
            new_name = request.POST.get('new_name')
            name = request.POST.get('name')
            if(name==new_name):
                print("inside name == newname")

                id = int(request.POST.get('id'))
                sold = int(request.POST.get('sold'))
                stock = int(request.POST.get('stock_left'))
                price = float(request.POST.get('price'))
                discount = float(request.POST.get('discount'))
                active = request.POST.get("yes_no")
                revenue = float(request.POST.get('price')) * int(request.POST.get('sold'))
                per = 100-(sold/(stock+sold))*100

                data = inventory.objects.get(id=id)
                data.Name = name
                data.sold = sold
                data.in_stock = stock
                data.price = price
                data.discount = discount
                data.revenue = revenue
                if stock<=0:
                    data.active = 0
                else:
                    data.active = active
                data.per = per
                data.save()

                context={
                    "title":"Update",
                    "data":"",
                    "msg":"1",
                }
                return render(request, "inventory_manager/update.html", context) 

            elif(name!=new_name):
                print("inside name != new_name")
                all_data = inventory.objects.all()
                for i in all_data:
                    if(i.Name == new_name):
                        flag = 1
                print("after for loop")
                if(flag==0):
                    print("inside flag = 0")
                    id = int(request.POST.get('id'))
                    sold = int(request.POST.get('sold'))
                    stock = int(request.POST.get('stock_left'))
                    price = float(request.POST.get('price'))
                    revenue = float(request.POST.get('price')) * int(request.POST.get('sold'))
                    active = request.POST.get("yes_no")
                    per = 100-(sold/(stock+sold))*100

                    data = inventory.objects.get(id=id)
                    data.Name = new_name
                    data.sold = sold
                    data.in_stock = stock
                    data.price = price
                    data.active = active
                    data.revenue = revenue
                    data.per = per
                    data.save()

                    context={
                        "title":"Update", 
                        "data":"",
                        "msg":"1"
                        }
                    return render(request, "inventory_manager/update.html", context) 
                else:
                    print("inside flag = 1")
                    context={
                        "title":"Update", 
                        "data":"",
                        "msg":"-1"
                        }
                    return render(request, "inventory_manager/update.html", context) 

    except:
        pass
        context={
                "title":"Edit",
                "data":"",
            }
    return render(request, "inventory_manager/update.html", context)

@login_required(login_url='/')
def get_names(request):
        term = request.GET.get('term')
        payload = []
        if term:
            objs = inventory.objects.filter(Name__istartswith = term)
            titles = list()
            for obj in objs:
                titles.append(obj.Name)
            return JsonResponse(titles, safe=False)
        return JsonResponse({
            'status': True,
            'payload': payload
        })

@login_required(login_url='/')
def sales_update(request):
    flag = 0
    try:
        if request.method == "GET":
            n = request.GET.get('name')
            data = inventory.objects.filter(Name=n)
            context={
                "title":"Update",
                "data":data,
            }
            return render(request, "inventory_manager/sales_update.html", context)

        if request.method == "POST":
            id = request.POST.get("id")
            sold = int(request.POST.get('sold'))

            item = inventory.objects.get(id=id)
            if item != None:
                rem = item.in_stock - sold
                total = sold * item.price
                rev = total - (total * (item.discount/100)) 
                item.revenue = item.revenue+rev
                item.in_stock = rem
                item.sold = item.sold+sold
                item.per = (100-(item.sold))/((item.in_stock+item.sold)*100)
                if(rem!=0):
                    item.active = 1
                else:
                    item.active = 0
                item.save()
                context={
                    "title":"sales",
                    "msg":"1",
                }
                return render(request, "inventory_manager/sales_update.html", context)
            else:
                context={
                    "title":"sales",
                    "msg":"0",
                }
                return render(request, "inventory_manager/sales_update.html", context)

    except:
        pass
    context={
        "title":"sales",
        "msg":"-1",
        }
    return render(request, "inventory_manager/sales_update.html", context)
